from django.db.models import FileField

class CustomFileField(FileField):

    ##############################################################################
    # We overrride the file field to catch a potential upload error to a remote  #
    # file hosting service. If an error occur in the upload, then the model      #
    # should not be saved.                                                       #
    ##############################################################################

    def pre_save(self, model_instance, add):
        # Returns field's value just before saving.
        file = super(CustomFileField, self).pre_save(model_instance, add)
        if file and not file._committed:
            # Commit the file to storage prior to saving the model
            try:
                file.save(file.name, file, save=False)
            except:
                return None
        return file