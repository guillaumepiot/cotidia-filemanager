# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import filemanager.fields


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='File',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('upload', filemanager.fields.CustomFileField(upload_to=b'uploads/%Y/%m/%d/', verbose_name='file')),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_updated', models.DateTimeField(auto_now=True)),
                ('is_image', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='FileToObject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('object_pk', models.TextField(null=True, verbose_name='object ID', blank=True)),
                ('caption', models.TextField(blank=True)),
                ('link_to_url', models.CharField(max_length=250, blank=True)),
                ('order_id', models.IntegerField(null=True, blank=True)),
                ('content_type', models.ForeignKey(related_name='content_type_set_for_filetoobject', verbose_name='content type', blank=True, to='contenttypes.ContentType', null=True)),
                ('file', models.ForeignKey(to='filemanager.File')),
            ],
        ),
    ]
