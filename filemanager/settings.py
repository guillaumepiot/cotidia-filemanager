from django.conf import settings

# Enable inline images for pages
FILE_THUMBNAIL_SIZE = getattr(settings, 'FILE_THUMBNAIL_SIZE', [100, 100])
FILE_CROP_SIZE = getattr(settings, 'FILE_CROP_SIZE', None) # [width, height]