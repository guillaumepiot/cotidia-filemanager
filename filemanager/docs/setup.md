Setup
=====

# Install

Edit mode:

$ pip install -e git+https://bitbucket.org/guillaumepiot/cotidia-filemanager.git#egg=filemanager

Production mode:

$ pip install git+https://bitbucket.org/guillaumepiot/cotidia-filemanager.git

# Migrate

$ python manage.py migrate filemanager

# Add multiple uploader widget to an admin form

	...
	# Import the widget
	from filemanager.widgets import MultipleFileWidget
	...

	# Add the widget to the form
	class PageFormAdmin(forms.ModelForm):
    	images = forms.CharField(widget=MultipleFileWidget, required=False)
    	class Meta:
        	model = Page

        # Initialise the form with the current object content type and id (passed to the widget attrs)
        # This is required to associate the file with the object
		def __init__(self, *args, **kwargs):
	        from django.contrib.contenttypes.models import ContentType
	        super(PageFormAdmin, self).__init__(*args, **kwargs)

	        if self.instance:
	            content_type = ContentType.objects.get_for_model(self.instance)
	            object_pk = self.instance.id
	            self.fields['images'].widget.attrs.update({'content_type':content_type.id, 'object_pk':object_pk})
	        else:
	            self.fields['images'].widget.attrs.update({'content_type':False, 'object_pk':False})


# Add the urls to your project

	...
	# File uploads
    url(r'^uploads/', include('filemanager.urls')),
    ...