import mimetypes

from PIL import Image, ImageOps
from io import BytesIO

from django.core.files.storage import default_storage
from django.core.files.base import ContentFile

from filemanager import settings as file_settings


def crop_image(file, name):

    # Handle errors due to image not fully completed
    # If a user download an image and upload is back before is fully
    # downloaded, PIL will throw a blocking error.
    # This work around will fill the missing part with grey

    try:
        image_file = Image.open(file)
    except:
        return

    try:
        image_file.load()
    except IOError:
        pass # You can always log it to logger

    cropped_image = ImageOps.fit(
        image_file, file_settings.FILE_CROP_SIZE, Image.ANTIALIAS)

    mime = mimetypes.guess_type(file.name)[0]
    data = BytesIO()
    format_string = mime.split('/')[1]
    cropped_image.save(data, format=format_string)
    default_storage.save(name, ContentFile(data.getvalue()))


def make_thumbnail(file, thumbnail_name):

    # Handle errors due to image not fully completed
    # If a user download an image and upload is back before is fully
    # downloaded, PIL will throw a blocking error.
    # This work around will fill the missing part with grey

    try:
        thumb_image = Image.open(file)
    except:
        return

    try:
        thumb_image.load()
    except IOError:
        pass # You can always log it to logger

    thumb_image.thumbnail(file_settings.FILE_THUMBNAIL_SIZE, Image.ANTIALIAS)

    mime = mimetypes.guess_type(file.name)[0]
    data = BytesIO()
    format_string = mime.split('/')[1]
    thumb_image.save(data, format=format_string)
    default_storage.save(thumbnail_name, ContentFile(data.getvalue()))
