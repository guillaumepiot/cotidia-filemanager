import json, urllib

from PIL import ImageOps

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView, DeleteView, ListView
from django.shortcuts import get_object_or_404
from django.contrib.contenttypes.models import ContentType
from django.core.files.storage import default_storage

from filemanager.models import File, FileToObject
from filemanager.serialize import serialize
from filemanager.utils import make_thumbnail, crop_image
from filemanager.forms import CaptionForm
from filemanager.settings import FILE_THUMBNAIL_SIZE, FILE_CROP_SIZE


@csrf_exempt
@require_POST
@login_required
def upload_photos(request):
    images = []
    for f in request.FILES.getlist("file"):
        obj = File.objects.create(upload=f, is_image=True)
        #images.append({"filelink": obj.upload.url})
        images = {"filelink": obj.upload.url}
    return HttpResponse(json.dumps(images), content_type="application/json")

@csrf_exempt
@require_POST
@login_required
def upload_files(request):
    files = []
    for f in request.FILES.getlist("file"):
        obj = File.objects.create(upload=f, is_image=False)
        #files.append({"filelink": obj.upload.url})
        files = {"filelink": obj.upload.url}
    return HttpResponse(json.dumps(files), content_type="application/json")


@login_required
def recent_photos(request):
    images = [
        {"thumb": obj.upload.url, "image": obj.upload.url}
        for obj in File.objects.filter(is_image=True).order_by("-date_created")[:20]
    ]
    return HttpResponse(json.dumps(images), content_type="application/json")


########################
# Multiple file upload #
########################


# UPLOAD

class PictureCreateView(CreateView):
    model = File

    # def post(self, request):
    #     print request.POST
    @method_decorator(login_required)
    def render_to_json_response(self, context, **response_kwargs):
        data = json.dumps(context)
        response_kwargs['content_type'] = 'application/json'
        return HttpResponse(data, **response_kwargs)

    def form_invalid(self, form):
        return self.render_to_json_response(form.errors, status=400)

    def form_valid(self, form):
        # Save the file
        self.object = form.save()

        # Associate with object if required
        if self.request.POST['content_type'] and self.request.POST['object_pk']:

            try:
                object_pk = int(self.request.POST['object_pk'])
                content_type = int(self.request.POST['content_type'])
            except:
                raise Exception('content_type and object_pk are not valid')

            ct = ContentType.objects.get_for_id(content_type)
            content_object = ct.get_object_for_this_type(pk=object_pk)
            file_to_object = FileToObject.objects.create(file=self.object, content_object=content_object)   

        # Crop image to fit aspect ratio
        if FILE_CROP_SIZE:
            cropped_image = crop_image(self.object.upload.file, self.object.upload.name)
            make_thumbnail(default_storage.open(self.object.upload.name), self.object.thumbnail_name)
        else:
            # Make thumbnail
            make_thumbnail(self.object.upload.file, self.object.thumbnail_name)

        files = [serialize(self.object)]
        data = {'files': files}
        response = HttpResponse(json.dumps(data), content_type="application/json")
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response


class BasicVersionCreateView(PictureCreateView):
    template_name_suffix = '_angular_full'
    fields = '__all__'


# DELETE
@login_required
def delete(request, file_id, content_type_id=False, object_pk=False):
    # self.object = self.get_object()
    # self.object.delete()
    if request.method == 'POST':

        if content_type_id and object_pk:
            f = get_object_or_404(FileToObject, id=file_id, content_type__id=content_type_id, object_pk=object_pk)
            f.delete()
        else:
            f = get_object_or_404(File, id=file_id)

        response = HttpResponse(json.dumps(True), content_type="application/json")
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response
    else:
        return HttpResponse("POST only please.")


# LIST
@login_required
def list(request, content_type_id=False, object_pk=False):
    if content_type_id and object_pk:
        files = [ serialize(p) for p in FileToObject.objects.filter(content_type__id=content_type_id, object_pk=object_pk).order_by('order_id') ]
    else:
        files = [ serialize(p) for p in File.objects.filter().order_by('order_id') ]
    data = json.dumps(files)
    response = HttpResponse(data, content_type="application/json")
    #response['Content-Disposition'] = 'inline; filename=files.json'
    return response

# LIBRARY
@login_required
def library(request, content_type_id=False, object_pk=False):
    if content_type_id and object_pk:
        associated_files = [f.file.id for f in FileToObject.objects.filter(content_type__id=content_type_id, object_pk=object_pk)]
        files = [ serialize(p) for p in File.objects.filter(is_image=True).exclude(id__in=associated_files).order_by('-date_created') ]
    else:
        files = [ serialize(p) for p in File.objects.filter(is_image=True).order_by('-date_created') ]
    data = json.dumps(files)
    response = HttpResponse(data, content_type="application/json")
    #response['Content-Disposition'] = 'inline; filename=files.json'
    return response

# ORDERING
@login_required
def order_files(request, content_type_id=False, object_pk=False):
    errors={}
    success=False
    file_order = []
    if request.method == "POST":
        for k,v in dict(request.POST).items():
            if k == 'file_order[]':
                file_order = v

        
        i = 0
        for file_id in file_order:
            # Get all the staff from our company
            f = get_object_or_404(FileToObject, id=file_id, content_type__id=content_type_id, object_pk=object_pk)
            f.order_id = i
            f.save()
            i=i+1
        success = True

    if content_type_id and object_pk:
        files = [ serialize(p) for p in FileToObject.objects.filter(content_type__id=content_type_id, object_pk=object_pk).order_by('order_id') ]
    else:
        files = [ serialize(p) for p in File.objects.filter().order_by('order_id') ]
    
    return HttpResponse(
        json.dumps(files),
        content_type = 'application/json'
    )

# GET CAPTION
@login_required
def get_caption(request, file_id, content_type_id, object_pk):
    errors={}
    success=False
    f = get_object_or_404(FileToObject, content_type__id=content_type_id, object_pk=object_pk, id=file_id)

    results = {'caption':f.caption, 'link_to_url':f.link_to_url}

    return HttpResponse(
        json.dumps(results),
        content_type = 'application/json'
    )

# GET CAPTION
@login_required
def choose_file(request, file_id, content_type_id, object_pk):
    errors={}
    success=False

    if request.method == "POST":
        f = get_object_or_404(File, id=file_id)

        ct = ContentType.objects.get_for_id(content_type_id)
        content_object = ct.get_object_for_this_type(pk=object_pk)
        file_to_object = FileToObject.objects.create(file=f, content_object=content_object)

    else:
        raise Exception("POST request only please.")
    

    results = True

    return HttpResponse(
        json.dumps(results),
        content_type = 'application/json'
    )

# SAVE CAPTION
@login_required
def save_caption(request, file_id, content_type_id, object_pk):
    errors={}
    success=False
    f = get_object_or_404(FileToObject, content_type__id=content_type_id, object_pk=object_pk, id=file_id)
    form = CaptionForm(instance=f)

    if request.method == "POST":
        form = CaptionForm(instance=f, data=request.POST)

        if form.is_valid():
            form.save()
            success=True
        else:
            for field in form:
                if field.errors:
                    errors[field.name] = field.errors

    else:
        raise Exception("POST request only please.")

    results = {'error':errors, 'success':success}

    return HttpResponse(
        json.dumps(results),
        content_type = 'application/json'
    )
