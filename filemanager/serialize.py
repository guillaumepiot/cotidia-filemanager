# encoding: utf-8
import mimetypes
import re
from django.core.urlresolvers import reverse
from django.conf import settings

def order_name(name):
    """order_name -- Limit a text to 20 chars length, if necessary strips the
    middle of the text and substitute it for an ellipsis.

    name -- text to be limited.

    """
    name = re.sub(r'^.*/', '', name)
    if len(name) <= 20:
        return name
    return name[:10] + "..." + name[-7:]


def serialize(instance):
    """serialize -- Serialize a Picture instance into a dict.

    instance -- Picture instance
    file_attr -- attribute name that contains the FileField or ImageField

    """
    

    if instance.__class__.__name__ == 'FileToObject':
        obj = getattr(instance, 'file')
        storage = obj.upload.storage
        mimetype = mimetypes.guess_type(storage.url(obj.upload.name))

        try:
            size = obj.upload.size
        except:
            size = False
            if settings.DEBUG:
                pass
                #raise Exception('Could not get the size of %s' % instance.file.upload.name)

        return {
            'id':instance.id,
            'url': obj.upload.url,
            'name': order_name(obj.upload.name),
            'type': mimetype[0],
            'thumbnailUrl': obj.thumbnail_url,
            'size': size,
            'deleteUrl': reverse('upload-delete', args=[instance.pk]),
            'deleteType': 'DELETE',
        }
    elif instance.__class__.__name__ == 'File':
        obj = getattr(instance, 'upload')
        storage = obj.storage
        mimetype = mimetypes.guess_type(storage.url(obj.name))

        try:
            size = obj.size
        except:
            if settings.DEBUG:
                raise Exception('Could not get the size of %s' % instance.upload.name)
            else:
                size = False

        return {
            'id':instance.id,
            'url': obj.url,
            'name': order_name(obj.name),
            'type': mimetype[0],
            'thumbnailUrl': instance.thumbnail_url,
            'size': size,
            'deleteUrl': reverse('upload-delete', args=[instance.pk]),
            'deleteType': 'DELETE',
        }
