/*
 * jQuery File Upload Plugin Angular JS Example 1.2.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2013, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/*jslint nomen: true, regexp: true */
/*global window, angular */

var url = '/uploads/basic/';

angular.module('multipleUploader', [
'blueimp.fileupload'
])
// .run(['$rootElement', function ($rootElement) {
//     this.formData = {
//         'content_type': $rootElement.find('input[name="content_type"]').val(),
//         'object_pk': $rootElement.find('input[name="object_pk"]').val()
//     }
//     console.log(this.formData);
// }])
.config([
    '$httpProvider', 'fileUploadProvider', '$routeProvider',
    function ($httpProvider, fileUploadProvider, $routeProvider) {

        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        fileUploadProvider.defaults.redirect = window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        );
        // if (isOnGitHub) {
        //     // Demo settings:
        //     angular.extend(fileUploadProvider.defaults, {
        //         // Enable image resizing, except for Android and Opera,
        //         // which actually support image resizing, but fail to
        //         // send Blob objects via XHR requests:
        //         disableImageResize: /Android(?!.*Chrome)|Opera/
        //             .test(window.navigator.userAgent),
        //         maxFileSize: 5000000,
        //         acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
        //     });
        // }



        angular.extend(fileUploadProvider.defaults, {
           beforeSend: function(xhr, settings) {
                xhr.setRequestHeader("X-CSRFToken", $('input[name="csrfmiddlewaretoken"]').val());
            },
            sequentialUploads: true, //One file at a time, one after the other
            singleFileUploads: true,
            imageMaxWidth: 50,
            // The maximum height of resized images:
            imageMaxHeight: 50,
            // The maximum width of the preview images:
            previewMaxWidth: 50,
            // The maximum height of the preview images:
            previewMaxHeight: 50
        });
        

    }
])

.config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('{[{');
  $interpolateProvider.endSymbol('}]}');
})

.controller('DemoFileUploadController', [
    '$scope', '$http', '$rootElement', 'fileUpload', '$filter', '$window', 
    function ($scope, $http, $rootElement, fileUpload) {

        $scope.options = {
            url: url,
            content_type: $rootElement.find('input[name="content_type"]').val(),
            object_pk: $rootElement.find('input[name="object_pk"]').val()
        };

        // Pass those variables to indicate where to save the files
        fileUpload.defaults.formData = {
            'content_type': $scope.options.content_type,
            'object_pk': $scope.options.object_pk
        }


        $scope.$on('fileuploadstop', function(event, data) {
            $rootElement.find('.fileuploader').modal('hide');
            $scope.fetch_images();
            $scope.queue = [];
            $('.progress').fadeOut();
        });
        $scope.$on('fileuploadstart', function(event, data) {
            $('.progress').show();
        });

        $scope.fetch_images = function(){
            $rootElement.find('.loading-images').show();
            $.get('/uploads/list/'+$scope.options.content_type+'/'+$scope.options.object_pk+'/', function(data){
                $scope.$apply(function () {
                    $scope.files = data
                    $rootElement.find('.loading-images').fadeOut();
                });
                $rootElement.find( ".sortable" ).sortable({
                  placeholder: "ui-state-highlight",
                  update: $scope.handleSortableUpdate
                });
            })
        }
        $scope.fetch_images();

        $scope.handleSortableUpdate = function(){
            // Show loader
            $('.loader').remove();
            $('body').append('<div class="loader"><span class="icon-spin5 animate-spin"></span> Updating order...</div>');
            var file_order = [];
            $rootElement.find('.sortable li').each(function() {
                    file_order.push($(this).attr('id').replace('file-',''));
            });
            var csrfmiddlewaretoken = $('input[name="csrfmiddlewaretoken"]').attr('value');
            var data = {file_order:file_order, csrfmiddlewaretoken:$('input[name="csrfmiddlewaretoken"]').val()};
            var jqxhr = $.post(
                    '/uploads/order-files/'+$scope.options.content_type+'/'+$scope.options.object_pk+'/',
                    data,
                    function(data){
                        $('.loader').html('<span class="glyphicon glyphicon-ok"></span> Order update complete').delay(2000).fadeOut(function(){
                            $(this).remove();
                        });
                    }
            )
            .error(function() { $('.loader').html("The file order could not be saved due to an error.") })


        // End of sortable update       
        }

        $scope.remove_file = function(file_id, file_url){
            $rootElement.find('.modal-delete-file').addClass('modal--open');
            $rootElement.find('.modal-delete-file .modal-image').html('<img class="thumbnail" src="'+file_url+'">');
            $rootElement.find('.remove-file').unbind().click(function(){
                $scope.do_remove_file(file_id);
            })
        }
        $scope.do_remove_file = function(file_id){
            var csrfmiddlewaretoken = $('input[name="csrfmiddlewaretoken"]').attr('value');
            var data = {csrfmiddlewaretoken:$('input[name="csrfmiddlewaretoken"]').val()};
            var jqxhr = $.post(
                    '/uploads/delete/'+file_id+'/'+$scope.options.content_type+'/'+$scope.options.object_pk+'/',
                    data,
                    function(data){
                        $rootElement.find('.modal-delete-file').removeClass('modal--open');
                        $scope.fetch_images();
                    }
            )
            .error(function() { $('.loader').html("The file order could not be saved due to an error.") })
        }

        $scope.file_caption = function(file_id, file_url){
            $rootElement.find('.modal-caption').addClass('modal--open');
            $rootElement.find('.modal-caption .modal-image').html('<img class="thumbnail" src="'+file_url+'">');
            $rootElement.find('.modal-caption textarea[name="caption"]').hide().before('<div class="alert alert-info">Loading current data</div>');
            $rootElement.find('.save-caption').unbind().click(function(){
                $scope.do_file_caption(file_id);
            })
            $.get(
                '/uploads/get-caption/'+file_id+'/'+$scope.options.content_type+'/'+$scope.options.object_pk+'/',
                function(data){
                    $rootElement.find('.modal-caption .alert').remove();
                    $rootElement.find('.modal-caption textarea[name="caption"]').val(data.caption).show();
                    $rootElement.find('.modal-caption input[name="link_to_url"]').val(data.link_to_url).show();

                }
            );
        }
        $scope.do_file_caption = function(file_id){
            var csrfmiddlewaretoken = $('input[name="csrfmiddlewaretoken"]').attr('value');
            var data = {csrfmiddlewaretoken:$('input[name="csrfmiddlewaretoken"]').val(), caption:$rootElement.find('.modal-caption textarea[name="caption"]').val(), link_to_url:$rootElement.find('.modal-caption input[name="link_to_url"]').val()};
            $.post(
                '/uploads/save-caption/'+file_id+'/'+$scope.options.content_type+'/'+$scope.options.object_pk+'/',
                data,
                function(data){
                    var formdiv = '.save-caption'

                    $rootElement.find(formdiv+' .alert').remove();
                    if(data.error){
                        $rootElement.find(formdiv+' #submit').attr('value','{% trans "Save" %}');
                        $.each(data.error, function(index, value) { 
                            $rootElement.find(formdiv+' [name="'+index+'"]').before('<div class="alert alert-error">'+value+'</div>');
                        })
                    }
                    if(data.success){
                        $rootElement.find(formdiv+' #submit').attr('value','{% trans "Save" %}');
                        $rootElement.find('.modal-caption').removeClass('modal--open');
                        //$scope.fetch_images();
                    }

                }
            )
            .error(function() { $('.loader').html("The file caption could not be saved due to an error.") })
        }

        $scope.open_library = function(){
            $rootElement.find('.modal-library').addClass('modal--open');
            $.get(
                '/uploads/library/'+$scope.options.content_type+'/'+$scope.options.object_pk+'/',
                function(data){
                    $scope.$apply(function () {
                        $scope.library = data;
                    });
                }
            );
        }

        $scope.do_choose_file = function(file_id){
            var csrfmiddlewaretoken = $('input[name="csrfmiddlewaretoken"]').attr('value');
            var data = {csrfmiddlewaretoken:$('input[name="csrfmiddlewaretoken"]').val()};
            $.post(
                '/uploads/choose-file/'+file_id+'/'+$scope.options.content_type+'/'+$scope.options.object_pk+'/',
                data,
                function(data){
                    $rootElement.find('.modal-library').removeClass('modal--open');
                    $scope.fetch_images();
                }
            )
            .error(function() { $('.loader').html("The file could not be associated due to an error.") })
        }


    }
])

.controller('FileDestroyController', [
    '$scope', '$http',
    function ($scope, $http) {
        var file = $scope.file,
            state;
        if (file.url) {
            file.$state = function () {
                return state;
            };
            file.$destroy = function () {
                state = 'pending';
                return $http({
                    url: file.deleteUrl,
                    method: file.deleteType,
                    headers: {
                      "X-CSRFToken": csrftoken
                   } 
                }).then(
                    function () {
                        state = 'resolved';
                        $scope.clear(file);
                    },
                    function () {
                        state = 'rejected';
                    }
                );
            };
        } else if (!file.$cancel && !file._index) {
            file.$cancel = function () {
                $scope.clear(file);
            };
        }
    }
]);

$(document).ready(function(){
    $('.add-files').click(function(){
        $(this).parent().find('.fileuploader').addClass('modal--open');
    })
    $('.modal__close, .cancel-modal').click(function(e){
        e.preventDefault()
        $('.modal').removeClass('modal--open');
    })
});


