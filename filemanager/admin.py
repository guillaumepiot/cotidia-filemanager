from django.contrib import admin
from django import forms
from django.conf import settings
from django.contrib.admin.actions import delete_selected as delete_selected_

from filemanager.models import File, FileToObject
from filemanager.widgets import AdminImageWidget, AdminCustomFileWidget

# Custom admin form
class FileAdminForm(forms.ModelForm):
    upload = forms.FileField(widget=AdminCustomFileWidget, required=False)
    class Meta:
        model=File
        exclude = ()

# Admin controller
class FileAdmin(admin.ModelAdmin):
    # Assign a custom form
    form = FileAdminForm
    actions = ['delete_selected']
    list_display = ['upload', 'image_preview', 'associated_with', 'date_created', 'is_image']

    def image_preview(self, obj):
        if obj.upload and obj.is_image:
            return '<img src="%s" alt="" width="22px" height="22px">' % (obj.upload.url)
        else:
            return ''
    image_preview.allow_tags = True
    image_preview.short_description = 'Preview'

    def associated_with(self, obj):
        related = FileToObject.objects.filter(file=obj)
        if related:
            associated_to = []
            for r in related:
                if r.content_object:
                    associated_to.append(r.content_object.__unicode__())
            return ", ".join(associated_to)
        else:
            return ''
    associated_with.allow_tags = True
    associated_with.short_description = 'Associated with'


    # FIELDSETS
    fieldsets = (
        ('File', {
        	'classes': ('default',),
            'fields': ('upload', 'is_image')
        }),
        
    )

    # def get_actions(self, request):
    #     actions = super(FileAdmin, self).get_actions(request)
    #     del actions['delete_selected']
    #     return actions

    def delete_selected(self, request, queryset):
        if not self.has_delete_permission(request):
            raise PermissionDenied
        if request.POST.get('post'):
            for obj in queryset:
                obj.delete()
        else:
            return delete_selected_(self, request, queryset)
    delete_selected.short_description = "Delete selected files"

admin.site.register(File, FileAdmin)


class FileToObjectAdmin(admin.ModelAdmin):
    list_display = ['file', 'caption', 'link_to_url', 'content_object']

admin.site.register(FileToObject, FileToObjectAdmin)