from django import forms

from filemanager.models import FileToObject

class CaptionForm(forms.ModelForm):
	class Meta:
		model = FileToObject
		fields = ['caption', 'link_to_url']
		exclude = ()