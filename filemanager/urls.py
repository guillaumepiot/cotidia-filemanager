from django.conf.urls import url

from filemanager import views

urlpatterns = [
    url(
        r"^ajax/photos/upload/$",
        views.upload_photos,
        name="upload_photos"),
    url(
        r"^ajax/files/upload/$",
        views.upload_files,
        name="upload_file"),
    url(
        r"^ajax/photos/recent/$",
        views.recent_photos,
        name="recent_photos"),

    url(
        r'^basic/$',
        views.BasicVersionCreateView.as_view(),
        name='upload-basic'),
    url(
        r'^delete/(?P<file_id>\d+)/(?P<content_type_id>\d+)/(?P<object_pk>\d+)/$',
        views.delete,
        name='upload-remove'),
    url(
        r'^delete/(?P<file_id>\d+)/$',
        views.delete,
        name='upload-delete'),
    url(
        r"^list/(?P<content_type_id>\d+)/(?P<object_pk>\d+)/$",
        views.list,
        name="list"),
    url(
        r"^all/$",
        views.list,
        name="list-all"),
    url(
        r"^order-files/(?P<content_type_id>\d+)/(?P<object_pk>\d+)/$",
        views.order_files,
        name="order-files"),
    url(
        r"^order-files/$",
        views.order_files,
        name="order-files-all"),
    url(
        r"^save-caption/(?P<file_id>\d+)/(?P<content_type_id>\d+)/(?P<object_pk>\d+)/$",
        views.save_caption,
        name="save-caption"),
    url(
        r"^get-caption/(?P<file_id>\d+)/(?P<content_type_id>\d+)/(?P<object_pk>\d+)/$",
        views.get_caption,
        name="get-caption"),
    url(
        r"^library/(?P<content_type_id>\d+)/(?P<object_pk>\d+)/$",
        views.library,
        name="library"),
    url(
        r"^choose-file/(?P<file_id>\d+)/(?P<content_type_id>\d+)/(?P<object_pk>\d+)/$",
        views.choose_file,
        name="choose-file"),
]
