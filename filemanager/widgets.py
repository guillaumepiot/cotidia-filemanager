import os

from django.contrib.admin.widgets import AdminFileWidget
from django.forms.widgets import HiddenInput
from django.utils.translation import ugettext as _
from django.utils.safestring import mark_safe
from django.template.loader import render_to_string

class AdminImageWidget(AdminFileWidget):
    def render(self, name, value, attrs=None):
        output = []
        
        if value and getattr(value, "url", None):
            image_url = value.url
            file_name=str(value)
            #image_url = '%s_%s%s' % (os.path.splitext(image_url)[0],280, os.path.splitext(image_url)[1])
            output.append(u' <a href="%s" target="_blank" class="image_link"><img title="Click to preview" src="%s" alt="%s" width="80px" /></a>' % (image_url, image_url, file_name))
        output.append(super(AdminFileWidget, self).render(name, value, attrs))

        return mark_safe('<div class="thumbnail admin-thumbnail">%s</div>' % u''.join(output))

    class Media:
        # css = {
        #     'all': ()
        # }
        js = (
            'js/load-image.js',
            )

class AdminCustomFileWidget(AdminFileWidget):
    def render(self, name, value, attrs=None):

        output = []
        
        if value and getattr(value, "url", None):
            image_url = value.url
            file_name=str(value)
            file_type = os.path.splitext(image_url)[1]
            file_type = file_type.replace('.','')
            #image_url = '%s_%s%s' % (os.path.splitext(image_url)[0],280, os.path.splitext(image_url)[1])
            if file_type in ['jpg', 'jpeg', 'png', 'gif']:
                output.append(u' <a href="%s" target="_blank" class="image_link"><img title="Click to preview" src="%s" alt="%s" width="80px" /></a>' % (image_url, image_url, file_name))
            else:
                output.append(u' <i class="file-icon-%s float-left margin-right"></i>' % (file_type))
        output.append(super(AdminFileWidget, self).render(name, value, attrs))

        return mark_safe('<div class="thumbnail admin-thumbnail">%s</div>' % u''.join(output))

class MultipleFileWidget(HiddenInput):
    def render(self, name, value, attrs=None):
        output = render_to_string('filemanager/file_angular.html', self.attrs)

        return mark_safe(output) 

    class Media:
        css = {
            'all': ('css/jquery.fileupload-ui.css', 'css/lightbox.css')
        }
        js = (
            'admin/js/angular.min.js',
            'js/vendor/jquery.ui.widget.js',
            'js/load-image.min.js',
            'js/canvas-to-blob.min.js',
            'js/jquery.iframe-transport.js',
            'js/jquery.fileupload.js',
            'js/jquery.fileupload-process.js',
            'js/jquery.fileupload-image.js',
            'js/jquery.fileupload-audio.js',
            'js/jquery.fileupload-video.js',
            'js/jquery.fileupload-validate.js',
            'js/jquery.fileupload-angular.js',
            'js/lightbox-2.6.min.js',
            'js/jquery-ui-sortable.min.js',
            'js/filemanager-app.js')