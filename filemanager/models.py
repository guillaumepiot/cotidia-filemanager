import datetime, os, mimetypes, urllib

from django.db import models
from django import forms
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from filemanager import settings as file_settings
from filemanager.fields import CustomFileField
from filemanager.utils import make_thumbnail

class FileToObject(models.Model):
    file = models.ForeignKey('File')
    # Content-object field
    # You can here hook any object the email may relate too
    content_type = models.ForeignKey(ContentType,
                    verbose_name=_('content type'),
                    related_name="content_type_set_for_%(class)s", blank=True, null=True)
    object_pk = models.TextField(_('object ID'), blank=True, null=True)
    content_object = GenericForeignKey(ct_field="content_type", fk_field="object_pk")
    caption = models.TextField(blank=True)
    link_to_url = models.CharField(max_length=250, blank=True)
    order_id = models.IntegerField(null=True, blank=True)

    @property
    def get_file_url(self):
        return self.file.upload.url

class File(models.Model):

    upload = CustomFileField(_('file'), upload_to="uploads/%Y/%m/%d/")
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)
    is_image = models.BooleanField(default=True)

    def __str__(self):
        return self.upload.name

    @property
    def thumbnail_name(self):
        outfile = '%s_%s%s' % (os.path.splitext(self.upload.name)[0], 'thumb', os.path.splitext(self.upload.name)[1])
        return outfile

    @property
    def thumbnail_url(self):

        if self.upload:
            storage = self.upload.storage

            if self.is_image and storage.exists(self.upload.name):

                # Create a thumbnail if it doesn't exist
                if not storage.exists(self.thumbnail_name):
                    make_thumbnail(self.upload.file, self.thumbnail_name)

                # Remove current name from url
                domain = self.upload.url.replace(self.upload.name, '')

                return "%s%s" % (domain, self.thumbnail_name)

        return None

    @models.permalink
    def get_absolute_url(self):
        return ('upload-new', )

    def save(self, *args, **kwargs):
        self.slug = self.upload.name
        storage = self.upload.storage
        mimetype = mimetypes.guess_type(storage.url(self.upload.name))
        if mimetype[0] in ['image/jpeg', 'image/bmp', 'image/gif', 'image/png', 'image/tiff']:
            self.is_image = True

        super(File, self).save(*args, **kwargs)




    def delete(self, *args, **kwargs):
        # You have to prepare what you need before delete the model
        storage = self.upload.storage
        # Delete the model before the file
        super(File, self).delete(*args, **kwargs)
        # Delete the file after the model
        try:
            storage.delete(self.upload.name)
        except:
            pass
        try:
            storage.delete(self.thumbnail_name)
        except:
            pass
